package org.killer_baba.dedsec_codebase.leetcode.topic_wise.arrays_1d;

//https://leetcode.com/problems/time-needed-to-buy-tickets/

public class Time_Needed_to_Buy_Tickets_2073 {
    public int timeRequiredToBuy(int[] tickets, int k) {
        int count = 0;
        int element = tickets[k];
        for (int i = 0; i < tickets.length; i++) {
            if(tickets[i]<element){
                count += tickets[i];
            }else {
                if(i<=k){
                    count += element;
                }else {
                    count += element-1;
                }
            }
        }
        return count;
    }
}
