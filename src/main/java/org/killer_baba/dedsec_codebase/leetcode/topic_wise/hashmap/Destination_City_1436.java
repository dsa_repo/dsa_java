package org.killer_baba.dedsec_codebase.leetcode.topic_wise.hashmap;

//https://leetcode.com/problems/destination-city/

import java.util.HashMap;
import java.util.List;

public class Destination_City_1436 {
    public String destCity(List<List<String>> paths) {
        HashMap<String ,String> map = new HashMap<>();
        for (List<String> lst : paths){
            map.put(lst.get(0),lst.get(1));
        }
        String city = paths.getFirst().getFirst();
        while (map.containsKey(city)){
            city = map.get(city);
        }
        return city;
    }
}
