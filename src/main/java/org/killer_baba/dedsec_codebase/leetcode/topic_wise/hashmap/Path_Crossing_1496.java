package org.killer_baba.dedsec_codebase.leetcode.topic_wise.hashmap;

//https://leetcode.com/problems/path-crossing/

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Path_Crossing_1496 {
    public boolean isPathCrossing(String path) {
        Set<String> coordSet = new HashSet<>();
        int x = 0,y = 0;
        coordSet.add(x+","+y);
        for (int i = 0; i < path.length(); i++) {
            char c = path.charAt(i);
            switch (c) {
                case 'N': y++; break;
                case 'S': y--; break;
                case 'E': x++; break;
                case 'W': x--; break;
            }
            System.out.println(x+","+y);
            if(coordSet.contains(x+","+y)){
                return true;
            }
            coordSet.add(x+","+y);
        }
        return false;
    }
}
