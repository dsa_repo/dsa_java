package org.killer_baba.dedsec_codebase.leetcode.topic_wise.two_pointer;

//https://leetcode.com/problems/assign-cookies/

import java.util.Arrays;

public class Assign_Cookies_455 {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int answer = 0;
        int m = 0, n = 0;
        while (m < g.length && n < s.length) {
            if(g[m] <= s[n]) {
                answer++;
                m++;
                n++;
            }else {
                n++;
            }
        }
        return answer;
    }
}
