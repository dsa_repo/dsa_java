package org.killer_baba.dedsec_codebase.leetcode.topic_wise.hashmap;

//https://leetcode.com/problems/redistribute-characters-to-make-all-strings-equal/description/

import java.util.HashMap;

public class Redistribute_Characters_to_Make_All_Strings_Equal_1897 {
    public boolean makeEqual(String[] words) {
        if(words.length == 1) return true;
        HashMap<Character, Integer> map = new HashMap<>();
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                map.put(word.charAt(i), map.getOrDefault(word.charAt(i), 0) + 1);
            }
        }
        for (Character character : map.keySet()) {
            if (map.get(character)%words.length != 0) {
                return false;
            }
        }
        return true;
    }
}
