package org.killer_baba.dedsec_codebase.leetcode.topic_wise.hashmap;

//https://leetcode.com/problems/find-words-that-can-be-formed-by-characters/

import java.util.HashMap;

public class Find_Words_That_Can_Be_Formed_by_Characters_1160 {

        public int countCharacters(String[] words, String chars) {
            int[] charArr = new int[26];
            int sum=0;
            for (int i = 0;i < chars.length(); i++){
                charArr[chars.charAt(i) - 'a'] +=1;
            }
            for (String str : words){
                HashMap<Character,Integer> temp = new HashMap<>();
                boolean flag = true;
                for (int i = 0; i < str.length(); i++) {
                    temp.put(str.charAt(i),temp.getOrDefault(str.charAt(i),0)+1);
                }
                for (Character ch : temp.keySet()){
                    if(temp.get(ch) > charArr[ch-'a']){
                        flag = false;
                        break;
                    }
                }
                if (flag){
                    sum += str.length();
                }
            }
            return sum;
        }

}
