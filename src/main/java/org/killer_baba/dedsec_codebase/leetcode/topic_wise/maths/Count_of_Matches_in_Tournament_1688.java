package org.killer_baba.dedsec_codebase.leetcode.topic_wise.maths;

//https://leetcode.com/problems/count-of-matches-in-tournament/

public class Count_of_Matches_in_Tournament_1688 {
    //log(n) Solution
    public int numberOfMatches(int n) {
        int answer = 0;
        while (n != 1) {
            if (n % 2 == 0) {
                answer += n / 2;
                n = n / 2;
            } else {
                answer += (n - 1) / 2;
                n = ((n - 1) / 2) + 1;
            }
        }
        return answer;
    }

    //O(1) Solution
    public int numberOfMatches2(int n) {
        return n-1;
    }

}
