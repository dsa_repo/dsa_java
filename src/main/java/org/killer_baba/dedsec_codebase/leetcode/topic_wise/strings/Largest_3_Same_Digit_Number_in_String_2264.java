package org.killer_baba.dedsec_codebase.leetcode.topic_wise.strings;

//https://leetcode.com/problems/largest-3-same-digit-number-in-string/


public class Largest_3_Same_Digit_Number_in_String_2264 {

        public String largestGoodInteger(String num) {
            String[] arr = new String[10];
            for (int i = 0; i < 10; i++) {
                arr[i] = i+""+i+i;
            }
            for (int i = 9; i >= 0; i--) {
                if(num.contains(arr[i])){
                    return arr[i];
                }
            }
            return "";
        }
}
