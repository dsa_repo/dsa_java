package org.killer_baba.dedsec_codebase.leetcode.topic_wise.hashmap;

//https://leetcode.com/problems/largest-substring-between-two-equal-characters/description/

import java.util.Arrays;

public class Largest_Substring_Between_Two_Equal_Characters_1624 {
    public int maxLengthBetweenEqualCharacters(String s) {
        int[] counts = new int[26];
        Arrays.fill(counts, -1);
        int maxlength = -1;
        for (int i = 0; i < s.length(); i++) {
            if(counts[s.charAt(i) - 'a'] > -1){
                maxlength = Math.max(maxlength,i-counts[s.charAt(i) - 'a']-1);
            }else {
                counts[s.charAt(i) - 'a'] = i;
            }
        }
        System.out.println(maxlength);
        return maxlength;
    }
}
