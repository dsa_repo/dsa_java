package org.killer_baba.dedsec_codebase.leetcode.topic_wise.strings;

//https://leetcode.com/problems/maximum-score-after-splitting-a-string/

public class Maximum_Score_After_Splitting_a_String_1422 {
    public int maxScore(String s) {
        int maxCount = 0,tempCount=0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                tempCount++;
            }
        }

        for (int i = 0; i < s.length()-1; i++) {
            if (s.charAt(i) == '0') {
                tempCount++;
            }
            if (s.charAt(i) == '1') {
                tempCount--;
            }
            if (tempCount > maxCount) {
                maxCount = tempCount;
            }
        }
        if (s.charAt(s.length()-1)=='1'){
            maxCount++;
        }
        return maxCount;
    }
}
