package org.killer_baba.dedsec_codebase.leetcode.topic_wise.arrays_2d;

//https://leetcode.com/problems/special-positions-in-a-binary-matrix/

public class Special_Positions_in_a_Binary_Matrix_1582 {
    public int numSpecial(int[][] mat) {
        int answer = 0;
        int[] mCounter = new int[mat.length];
        int[] nCounter = new int[mat[0].length];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if(mat[i][j] == 1){
                    mCounter[i]++;
                    nCounter[j]++;
                }
            }
        }
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if(mat[i][j] == 1 && mCounter[i] == 1 && nCounter[j] == 1){
                    answer++;
                }
            }
        }
        return answer;
    }
}
