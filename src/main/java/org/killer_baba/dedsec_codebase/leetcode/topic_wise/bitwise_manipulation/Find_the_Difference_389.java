package org.killer_baba.dedsec_codebase.leetcode.topic_wise.bitwise_manipulation;

//https://leetcode.com/problems/find-the-difference/

public class Find_the_Difference_389 {
    public char findTheDifference(String s, String t) {
        int c = 0;
        for (int i = 0; i < s.length(); i++) {
            c = c ^ s.charAt(i);
        }
        for (int i = 0; i < t.length(); i++) {
            c = c ^ t.charAt(i);
        }
        return (char) c;
    }
}
