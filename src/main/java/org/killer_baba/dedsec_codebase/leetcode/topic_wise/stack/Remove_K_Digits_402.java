package org.killer_baba.dedsec_codebase.leetcode.topic_wise.stack;

//https://leetcode.com/problems/remove-k-digits/

import java.util.Stack;

public class Remove_K_Digits_402 {
    public String removeKdigits(String num, int k) {
        if (num == null || num.isEmpty()){
            return "";
        }
        Stack<Character> stack = new Stack<>();
        stack.push(num.charAt(0));
        int counter=0;
        for (int i = 1; i < num.length(); i++) {
            char temp =num.charAt(i);
            while(counter<k && !stack.isEmpty() &&stack.peek() > temp){
                stack.pop();
                counter++;
            }
            stack.push(temp);
        }
        while (counter < k){
            stack.pop();
            counter++;
        }
        StringBuilder answer = new StringBuilder();
        for (Character j : stack){
            answer.append(j);
        }
        String str = answer.toString().replaceFirst("^0+","");
        if (str.isEmpty() || str.equals("0")) {
            str = "0";
        }
        return str;
    }
}
