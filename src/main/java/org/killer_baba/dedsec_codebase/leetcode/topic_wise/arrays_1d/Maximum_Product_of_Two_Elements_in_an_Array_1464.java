package org.killer_baba.dedsec_codebase.leetcode.topic_wise.arrays_1d;

//https://leetcode.com/problems/maximum-product-of-two-elements-in-an-array/

public class Maximum_Product_of_Two_Elements_in_an_Array_1464 {

    public int maxProduct(int[] nums) {
        int firstMax = 0,firstMaxIndice = 0, secondMax = 0;
        for (int i = 0; i < nums.length; i++) {
            if(firstMax < nums[i]){
                firstMax = nums[i];
                firstMaxIndice = i;
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if(secondMax < nums[i] && i != firstMaxIndice){
                secondMax = nums[i];
            }
        }
        return (firstMax-1)*(secondMax-1);
    }

}
