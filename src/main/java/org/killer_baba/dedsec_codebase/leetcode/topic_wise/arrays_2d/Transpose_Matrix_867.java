package org.killer_baba.dedsec_codebase.leetcode.topic_wise.arrays_2d;

//https://leetcode.com/problems/transpose-matrix/

public class Transpose_Matrix_867 {
    public int[][] transpose(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        System.out.println(m + " " + n);
        int[][] answerMatrix = new int[n][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                answerMatrix[j][i] = matrix[i][j];
            }
        }
        return answerMatrix;
    }
}
