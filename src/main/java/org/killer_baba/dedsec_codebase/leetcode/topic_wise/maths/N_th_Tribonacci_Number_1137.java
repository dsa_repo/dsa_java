package org.killer_baba.dedsec_codebase.leetcode.topic_wise.maths;

//https://leetcode.com/problems/n-th-tribonacci-number/description/?envType=daily-question&envId=2024-04-24

public class N_th_Tribonacci_Number_1137 {
    public int tribonacci(int n) {
        if(n==0){
            return 0;
        }
        if(n==1 || n==2){
            return 1;
        }
        int n0 = 0,n1=1,n2=1,answer=0;
        for (int i = 3; i <= n; i++) {
            answer = n0+n1+n2;
            n0=n1;
            n1=n2;
            n2=answer;
        }
        return answer;
    }
}
