package org.killer_baba.dedsec_codebase.leetcode.topic_wise.strings;

//https://leetcode.com/problems/minimum-changes-to-make-alternating-binary-string/


public class Minimum_Changes_To_Make_Alternating_Binary_String_1758 {
    public int minOperations(String s) {
        int way1 = checkIfStringStartWith1(s);
        int way2 = checkIfStringStartsWith0(s);
        return Math.min(way2,way1);
    }

    public int checkIfStringStartsWith0(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '1'){
                count++;
            }
            i++;
            if(i == s.length()){
                break;
            }
            if(s.charAt(i) == '0'){
                count++;
            }
        }
        return count;
    }

    public int checkIfStringStartWith1(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i+=2) {
            if(s.charAt(i) == '0'){
                count++;
            }
            i++;
            if(i == s.length()){
                break;
            }
            if(s.charAt(i) == '1'){
                count++;
            }
        }
        return count;
    }
}
