package org.killer_baba.dedsec_codebase.leetcode.vigilante_streaks.april_2024;

//https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/description/?envType=daily-question&envId=2024-04-06

public class Minimum_Remove_to_Make_Valid_Parentheses_1249_06 {
    public String minRemoveToMakeValid(String s) {
        if(s==null || s.isEmpty()){
            return "";
        }
        int count=0;
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '('){
                temp.append(s.charAt(i));
                count++;
            }else if(s.charAt(i) == ')' && count == 0){
                continue;
            } else if (s.charAt(i) == ')') {
                count--;
                temp.append(s.charAt(i));
            }else {
                temp.append(s.charAt(i));
            }
        }
        count=0;
        StringBuilder answer = new StringBuilder();
        for (int i = temp.length()-1; i >= 0; i--) {
            if(temp.charAt(i) == ')'){
                count++;
                answer.append(temp.charAt(i));
            }else if(temp.charAt(i) == '(' && count == 0){
                continue;
            } else if (temp.charAt(i) == '(') {
                count--;
                answer.append(temp.charAt(i));
            }else {
                answer.append(temp.charAt(i));
            }
        }
        return answer.reverse().toString();
    }
}
