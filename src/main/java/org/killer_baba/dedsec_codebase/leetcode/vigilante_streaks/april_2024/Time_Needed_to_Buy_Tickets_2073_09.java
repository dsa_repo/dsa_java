package org.killer_baba.dedsec_codebase.leetcode.vigilante_streaks.april_2024;

//https://leetcode.com/problems/time-needed-to-buy-tickets/description/?envType=daily-question&envId=2024-04-09
public class Time_Needed_to_Buy_Tickets_2073_09 {
    public int timeRequiredToBuy(int[] tickets, int k) {
        int count = 0;
        int element = tickets[k];
        for (int i = 0; i < tickets.length; i++) {
            if(tickets[i]<element){
                count += tickets[i];
            }else {
                if(i<=k){
                    count += element;
                }else {
                    count += element-1;
                }
            }
        }
        return count;
    }
}
