package org.killer_baba.dedsec_codebase.leetcode.vigilante_streaks.april_2024;

//https://leetcode.com/problems/valid-parenthesis-string/description/?envType=daily-question&envId=2024-04-07

import java.util.Iterator;
import java.util.LinkedList;

public class Valid_Parenthesis_String_678_07 {
    public boolean checkValidString(String s) {
        if (s==null){
            return false;
        }
        if(s.isEmpty()){
            return true;
        }
        int count=0,starCount=0;
        LinkedList<Character> counter = new LinkedList<>();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '(' || s.charAt(i) == '*'){
                counter.add(s.charAt(i));
            }else if(s.charAt(i) == ')'){
                boolean flag = false;
                if(counter.isEmpty()){
                    return false;
                }else {
                    Iterator<Character> iterator = counter.descendingIterator();
                    while (iterator.hasNext()){
                        if(iterator.next()=='('){
                            flag = true;
                            iterator.remove();
                            break;
                        }
                    }
                    if(!flag){
                        Iterator<Character> iterator1 = counter.descendingIterator();
                        while (iterator1.hasNext()){
                            if(iterator1.next()=='*'){
                                flag = true;
                                iterator1.remove();
                                break;
                            }
                        }
                    }
                    if(!flag){
                        return false;
                    }
                }
            }
        }
        int startemp=0;
        while (!counter.isEmpty()){
            char c = counter.removeLast();
            if (c == '*') {
                startemp++;
            } else if (c == '(') {
                startemp--;
            }
            if (startemp < 0) {
                return false;
            }
        }
        return true;
    }
}
